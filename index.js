const createError = require('micro').createError;
const url = require('url');
const handler = require('serve-handler');
const fetch = require('node-fetch');

const Switch = {
    new: async () => {
        const request = await fetch('https://api.keyvalue.xyz/new/switch', { method: 'POST' });
        const res = await request.text();
        const id = res.replace("https://api.keyvalue.xyz/", "").replace("/switch", "").replace("\n", "");
        return {id:id, status: "off"};
    },

    get: async (id) => {
        const request = await fetch('https://api.keyvalue.xyz/'+id+'/switch')
        var data = await request.text();
        data = data.replace("\n", "");
        if (data === "") data = "off";
        return {id:id, status: data};
    },

    switch: async (id) => {
        const s = await Switch.get(id);
        var value = (s.status === "off") ? "on" : "off";

        const request = await fetch('https://api.keyvalue.xyz/'+id+'/switch', { method: 'POST', body: value });
        const res = await request.text();
        return {id:id, status: value};
    }
}

async function api(parts, req, res) {
    
    if (parts[1] === "new") {
        if (req.method !== "POST") throw createError(405, 'Method Not Allowed');
        return await Switch.new();
    }

    if (parts[1] === "switch") {
        if (req.method === "PUT") {
            return await Switch.switch(parts[2]);
        } else if (req.method === "GET") {
            return await Switch.get(parts[2]);
        }
        throw createError(405, 'Method Not Allowed');
    }

    throw createError(404, 'Not Found');
}

module.exports = async (req, res) => {
    const parsed = url.parse(req.url);
    const parts = parsed.path.match(/^\/api\/v1\/([a-z]+)\/?([a-z0-9]*)/);

    if (parts) {
        return await api(parts, req, res);
    } else {
        await handler(req, res, {
            public: "public"
        });
    }
}